<?php 
get_header();

wp_enqueue_style( 'page-css' , get_stylesheet_directory_uri() . '/assets/css/pages.css' );

$banner_url = '';  
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
if ( has_post_thumbnail() ) {
	$banner_url = $image[0];
} else {
	$banner_url = get_stylesheet_directory_uri() . '/assets/img/slider/1.jpg';
}

?>
				<!-- Slider -->
				<div class="banner-wrapper">
					<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo $banner_url; ?>">
						<div class="parallax-window-content text-center">
							<div class="title-wrapper">
								<h4>Page Not Found</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<section id="main-container">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<article>
						<h3>Error 404: Page not found!</h3>
						<p>Sorry, the keyword you are looking for is not available. Maybe you want to perform a search?</p>
						
						<?php get_search_form(); ?>
						
						<br>
						<p>For best search results, mind the following suggestions:</p>
						<ul>
							<li>Always double check your spelling.</li>
							<li>Try similar keywords, for example: tablet instead of laptop.</li>
							<li>Try using more than one keyword.</li>
						</ul>							
					</article>
				</div>
				<div class="col-md-4">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>

		<!-- Advertisements -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<img src="http://placehold.it/1000x200?text=Advertisements+here" width="100%">
				</div>
			</div>
		</div>
	</section>


<?php get_footer(); ?>