<?php  

define( 'theme_url' , get_stylesheet_directory_uri() . '/' );
define('root_path',  get_stylesheet_directory() . '/' );

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

/* -----------------  Enqueue Assets -------------------------- */
function enqueue_assets() {
	// ENQUEUEING GLOBAL STYLESHEETS
	wp_enqueue_style( 'bootstrap-css'	, theme_url . 'assets/css/bootstrap.min.css' );
	wp_enqueue_style( 'font-awesome' 	, theme_url . 'assets/css/font-awesome.min.css' );
	wp_enqueue_style( 'animate' 		, theme_url . 'assets/css/animate.min.css' );
	wp_enqueue_style( 'normalize' 		, theme_url . 'assets/css/normalize.css' );
	wp_enqueue_style( 'responsive' 		, theme_url . 'assets/css/responsive.css' );
	wp_enqueue_style( 'theme-style', get_stylesheet_uri() );

	// ENQUEUEING GLOBAL SCRIPTS
	wp_enqueue_script( 'jquery-js'					, theme_url . 'assets/js/jquery.min.js', array( 'jquery' ) , '' , TRUE );
	wp_enqueue_script( 'bootstrap-js'				, theme_url . 'assets/js/bootstrap.min.js', array( 'jquery' ) , '' , TRUE );
	wp_enqueue_script( 'parallax'					, theme_url . 'assets/js/parallax.min.js', array( 'jquery' ) , '' , TRUE );
	wp_enqueue_script( 'wow'						, theme_url . 'assets/js/wow.min.js', array( 'jquery' ) , '' , TRUE );
	wp_enqueue_script( 'greensock'					, theme_url . 'assets/js/greensock.js', array( 'jquery' ) , '' , TRUE );
	wp_enqueue_script( 'layerslider-kreaturamedia'	, theme_url . 'assets/js/layerslider.kreaturamedia.jquery.js', array( 'jquery' , 'greensock' ) , '' , TRUE );
	wp_enqueue_script( 'layerslider-transitions'	, theme_url . 'assets/js/layerslider.transitions.js', array( 'jquery' ) , '' , TRUE );
	wp_enqueue_script( 'animatescroll'				, theme_url . 'assets/js/animatescroll.min.js', array( 'jquery' ) , '' , TRUE );
	wp_enqueue_script( 'plugins'					, theme_url . 'assets/js/plugins.js', array( 'jquery' ) , '' , TRUE );
	wp_enqueue_script( 'scripts'					, theme_url . 'assets/js/scripts.js', array( 'jquery' ) , '' , TRUE );
}
function include_js( $url_path ){
	echo '<script type="text/javascript" src="' . theme_url . '/' .$url_path.'"></script>'."\n";
}
/* -----------------  Gets the Theme name --------------------- */
function get_theme_name() {
	$name = str_replace(dirname(dirname(__FILE__)),'',dirname(__FILE__));
	$name = ltrim($name,'\\');	
	return $name;
}

/* -----------------  Register Menus in Admin Page ------------- */
function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}

/* -----------------  Add class to link tag in menu ------------- */
function add_menuclass($ulclass) {
	return preg_replace('/<a"/ ', '<a class="hvr-underline-from-center"', $ulclass, 1);
}

add_filter('wp_nav_menu', 'custom_menu_display');
function custom_menu_display($output) {
	echo preg_replace('| class="dropdown-toggle" data-toggle="dropdown" data-target="#"(.+?)<b |', ' class="a-stripped" $1</a><a href="#" class="dropdown-toggle a-caret" data-toggle="dropdown" data-target="#"><b ', $output, -1);
}
/* -----------------  Register Sidebar -------------------------- */
function arphabet_widgets_init() {
	register_sidebar(array(
		'name' => __('Main sidebar', 'framewrok'),
		'id' => 'home_right_1',
		'description' => __('Default main sidebar.', 'framework'),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-title"><h2>',
		'after_title' => '</h2></div>'
	));
}

/* ----------------- Customize Read More in Excerpt ------------ */
function excerpt_readmore( $more ) {
	return ' ...';
}
function search_page_text_length($length) {
    return 231;
}

/* ----------------- Display Child Pages----------- ------------ */
function wpb_list_child_pages() { 

	global $post; 

	if ( is_page() && $post->post_parent )
		$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
	else
		$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
	if ( $childpages ) {
		$string = '<ul>' . $childpages . '</ul>';
	}

	return $string;
}

add_shortcode('wpb_childpages', 'wpb_list_child_pages');


/* ----------------- Add HTML5 theme support Search Form.----------- ------------ */
function search_form_no_filters() {
	// look for local searchform template
	$search_form_template = locate_template( 'searchform.php' );
	if ( '' !== $search_form_template ) {
		// searchform.php exists, remove all filters
		remove_all_filters('get_search_form');
	}
}
add_action('pre_get_search_form', 'search_form_no_filters');

/* -----------------  Initiate registered functions ------------ */
add_action( 'init', 'register_my_menu' );
// add_filter('wp_nav_menu','add_menuclass');
add_theme_support( 'post-thumbnails' );
add_action( 'widgets_init', 'arphabet_widgets_init' );
add_action( 'wp_enqueue_scripts', 'enqueue_assets' );

/* ----------------- Initiate Read More in Excerpt ------------- */
add_filter( 'excerpt_more', 'excerpt_readmore' );
add_filter('excerpt_length', 'search_page_text_length');

include "inc/widgets.php";
include "inc/class-tgm-plugin-activation.php";
include "inc/theme-options.php";
include "inc/tgm-plugin-activation.php";
include "inc/custom-header.php";
include "init/wp-bootstrap-navwalker.php";
include  root_path.'functions-custom.php';
?>