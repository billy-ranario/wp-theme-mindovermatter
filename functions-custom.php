<?php 
/*
* This displays the homepage slider of the site
*
*/
function homepage_slider( ) {
	
	$params = array(
		'limit' => 7
	);

	$pods_object = pods( 'homepage_slider' , $params ) ;
	
	if ( $pods_object ) {

		while ( $pods_object->fetch() ) {
			$slide_image 		= 	$pods_object->display( 'slide_image' );
			$event_date 		= 	date("M d, Y", strtotime( $pods_object->field( 'event_date' ) ));
			$image_alt 			= 	$pods_object->field( 'image_alt' );
			$event_title	 	= 	$pods_object->field( 'event_title' );
			$event_description 	= 	$pods_object->field( 'event_description' );
			$read_more 			= 	$pods_object->field( 'read_more' );

			// strip tags to avoid breaking any html
			$event_description = strip_tags($event_description);

			if (strlen($event_description) > 240) {

			    // truncate string
			    $stringCut = substr($event_description, 0, 240);

			    // make sure it ends in a word so assassinate doesn't become ass...
			    $event_description = substr($stringCut, 0, strrpos($stringCut, ' ')).' ...'; 
			}

			?>					


				<div class="ls-slide" 
			    	data-ls="
			    		transition2d: all;
			    		slidedelay: 4000"
			    		>
					<div class="ls-l texts-layer hidden-sm hidden-xs img-content-slider slider-img-1" data-ls="offsetxin:left;">
						<?php  
				        if ( !$slide_image ) {
				        	$slide_image =  get_stylesheet_directory_uri() . '/assets/img/slider/1.jpg';
				        }
				        ?>
						<img alt="<php echo $image_alt; ?>" src="<?php echo $slide_image; ?>" class=" ls-bg " data-ls="transition2d: all;">
						<img alt="<php echo $image_alt; ?>" src="<?php echo $slide_image; ?>" class="ls-tn">	
					</div>
					<div class="ls-l texts-layer hidden-sm hidden-xs texts-date text-center" data-ls="offsetxin:0;durationin:3000;delayin:700;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">
						<span class="date">
							<?php echo $event_date; ?>
						</span>
					</div>
					<div class="ls-l texts-layer texts-event-title text-center slider-text-1 hidden-sm hidden-xs" data-ls="offsetxin:left;delayin:1000">
						<h1><?php echo $event_title ?></h1>
					</div>
					<div class="ls-l texts-layer texts-event-description texts-content slider-text-1 hidden-sm hidden-xs" data-ls="offsetxin:right;delayin:1200">
						<p>
							<?php echo $event_description ?>
						</p>
					</div>
					<div class="ls-l texts-layer hidden-sm hidden-xs texts-btn text-center" data-ls="offsetxin:0;durationin:3000;delayin:700;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">
						<a class="readmore" href="<?php echo $read_more; ?>">
							Read More
						</a>
					</div>
				</div>


		<?php }
		}

	$result = ob_get_contents();
	ob_end_clean(); 
	wp_reset_query();
	
	return $result;
}


/*
* This displays the homepage slider of the site
*
*/
function front_page_box()
{
	$params = array(
		'limit' => 7
	);

	$pods_object = pods( 'picture_links' , $params ) ;
	
	if ( $pods_object ) {

		while ( $pods_object->fetch() ) {
			$box_image 			= 	$pods_object->display( 'box_image' );
			$image_alt	 			= 	$pods_object->field( 'image_alt' );
			$title 				= 	$pods_object->field( 'image_title' );
			$link	 			= 	$pods_object->field( 'image_link' );
			?>					
			<div class="col-md-4 col-sm-4 col-xs-12 column_service">
				<div class="box-wrapper">
					<a href="<?php echo $link ?>">
						<div class="image-box">
							<img src="<?php echo $box_image; ?>" alt="<?php echo $image_alt; ?>" class="fullwidth">
						</div>
						<div class="title-overlay">
							<h2><?php echo $title ?></h2>
						</div>
					</a>
				</div>
			</div>
		<?php }
		}

	$result = ob_get_contents();
	ob_end_clean(); 
	wp_reset_query();
	
	return $result;	
}
/*
* This loops the page contents
*
*/

global $image_src;

function page_content() {
	if ( have_posts() ) :
		while ( have_posts( ) ) : the_post();
			the_content();
		endwhile; 
	endif;

	$result = ob_get_contents();
	ob_end_clean(); 
	wp_reset_query();
	
	return $result;
}

/*
* Displays searched contents
*
*/
function page_searched_content() {
	echo '<div class="main_post wow fadeInUp" data-wow-duration="2s">';
	if ( have_posts() ) :
		while ( have_posts( ) ) : the_post();
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
	?>
			<div class="searched_wrapper">
				<div class="searched-title" style="margin-bottom: 20px;">
					<h3 style="margin-bottom:0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<small class="text-warning"><i class="fa fa-user"></i> &nbsp; <i>by</i> <?php the_author(); ?></small>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<article class="text-justify">
							<?php the_excerpt(); ?>
						</article>
					</div>
				</div>
			</div>
	<?php
		endwhile; 
	endif;
	echo '</div>';

	$result = ob_get_contents();
	ob_end_clean(); 
	wp_reset_query();
	
	return $result;
}

/*
* Load CSS for common pages
* disabled
*/
function page_stylesheet() {
	echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri() .'/css/page.css">';
}


/*
* This displays the footer logos
*
*/
function display_bottom_img() {
	$params = array(
		'limit' => 150,
		'order'=>'ASC'
	);

	$pods_object = pods( 'bottom_images' , $params );
	while ( $pods_object->fetch() ) {
		$bottom_logo = $pods_object->display( 'bottom_image' );
	?>
		<div class="col-md-3 col-sm-3 col-xs-6 box-content wow fadeIn">
			<div class="img-wrapper">
				<img src="<?php echo $bottom_logo; ?>" class="img-responsive">
			</div>						
		</div>
	<?php
	}
}


/*
* Custom Excerpt
*
*/
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
	array_pop($excerpt);
	$excerpt = implode(" ",$excerpt).'...';
	} else {
	$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
	return $excerpt;
}

function content($limit) {
	$content = explode(' ', get_the_content(), $limit);
	if (count($content)>=$limit) {
	array_pop($content);
	$content = implode(" ",$content).'...';
	} else {
	$content = implode(" ",$content);
	}
	$content = preg_replace('/[.+]/','', $content);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}