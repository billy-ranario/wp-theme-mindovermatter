<div class="sidebar">
	<div class="row_sb">
		<h3>Recent Activities</h3>
		<div class="row row_lists">


			<?php
				$args = array( 'numberposts' => '5' , 'offset' => '3' ); //Offset - to exclude 3 latest post 
				$recent_posts = wp_get_recent_posts( $args );
				$img_thumbnail = '';  

				if ( count($recent_posts) > 0 ) {					
					foreach( $recent_posts as $recent ){
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $recent["ID"] ), 'single-post-thumbnail' ); 
						
						if ( has_post_thumbnail( $recent["ID"] ) ) {
							$img_thumbnail = $image[0];
						 } else {
							$img_thumbnail = get_stylesheet_directory_uri() . '/assets/img/thumbnail-default.jpg';
						}
					?>
						<div class="post_list">
							<div class="col-md-5 col-sm-6 col-xs-12">
								<div class="thumbnail-post">
									<a href="<?php the_permalink(); ?>"><img src="<?php echo $img_thumbnail; ?>" class="fullwidth"></a>
								</div>
							</div>
							<div class="col-md-7 col-sm-6 col-xs-12">
								<div class="post-desc">
									<div class="title-post">
										<a href="<?php the_permalink(); ?>"><h4><?php echo  __( $recent["post_title"] ); ?></h4></a>
									</div>
									<div class="post-text">
										<p><?php echo excerpt(20); ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="separator"></div>
							</div>
						</div>
					<?php
					}
				} else { ?>
					<div class="post_list">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="thumbnail-post">
								<h2>No Recent Post Available</h2>
							</div>
						</div>
					</div>
			<?php	}
			?>
		</div>
	</div>
</div>