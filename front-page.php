<?php 
	// LOAD STYLESHEET FOR WEBSITE FRONT PAGE
	wp_enqueue_style( 'homepage' , get_stylesheet_directory_uri() . '/assets/css/homepage.css' ); 
	wp_enqueue_style( 'layerslider' , get_stylesheet_directory_uri() . '/assets/css/layerslider.css' );
?>

<?php get_header();  ?>


				<!-- Slider -->
				<div class="banner-wrapper">
					<div class="banner-content" id="banner" style="height:620px;">
						<?php echo homepage_slider(); ?>
					</div>
				</div>
			</div>
		</div>
	</header>

	<section id="main-container" class="frontpage">
		<div class="container">
			<article>
				<h1><?php bloginfo( 'name' ); ?> <small class="text-line"><span class="fa fa-dribbble"></span></small></h1>

				<?php echo page_content(); ?>
				
			</article>
		</div>

		<div class="container">
			<article>
				<?php  
				
				$page = get_page_by_title( 'Our Mission' ); //as an e.g.

				$the_query = new WP_Query( 'page_id='.$page->ID );
				while ( $the_query->have_posts() ) :
					$the_query->the_post();
				?>
					<h1><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?> <small class="text-line"><span class="fa fa-dribbble"></span></small></a></h1>
					<div class="row">
						<?php the_content(); ?>
					</div>
					<?php
				endwhile;
				wp_reset_postdata();
				?>
			</article>
		</div>

		<div class="container">
			<div class="row">
				<?php echo front_page_box(); ?>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<article>
						<h1>Upcoming Events <small class="text-line"><span class="fa fa-dribbble"></span></small></h1>

						<?php 
							// query_posts('category_name=News,Uncategorized');
							query_posts( 'posts_per_page=-1' );
							if( have_posts() ):
								while (have_posts()) : the_post();
									// $img_thumbnail = '';  
									
									$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
									if ( has_post_thumbnail() ) {
										$img_thumbnail = $image[0];
									 } else {
										$img_thumbnail = get_stylesheet_directory_uri() . '/assets/img/thumbnail-default.jpg';
									}

						 ?>
									<div class="row row_post">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="post-img-wrapper" style="background-image:url('<?php echo $img_thumbnail ?>');">
											</div>
											<div class="post-img-date">
												<p>
													<i class="fa fa-calendar"></i> <?php the_time("M d Y") ; ?>
												</p>
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<h3><?php echo the_title(); ?></h3>
											<p>
												<?php echo excerpt(45); ?>
											</p>
											<p>
												<a href="<?php echo the_permalink(); ?>" class="theme-btn">More Details</a>
											</p>
										</div>
										<div class="col-md-12">
											<div class="separator"></div>
										</div>
									</div>		
						<?php
								endwhile;
							endif; 
							wp_reset_query();
						?>
					</article>
				</div>
				<div class="col-md-4">
					<article style="visibility: hidden;">
						<h1>a <small class="text-line"><span class="fa fa-dribbble"></span></small></h1>
					</article>
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>

		<!-- Advertisements -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<img src="http://placehold.it/1000x200?text=Advertisements+here" width="100%">
				</div>
			</div>
		</div>
	</section>
<?php get_footer() ?>
