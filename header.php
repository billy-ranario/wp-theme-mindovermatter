<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php bloginfo( 'name' ); ?></title>
	<?php  
		do_action( 'genesis_meta' ); // Get Meta Tags from GENESIS 
		wp_head(); 
	?>
	<link rel="shortcut icon" href="assets/img/logo.ico" type="image/x-icon">
	<link rel="icon" href="assets/img/logo.ico" type="image/x-icon">

<!-- 	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/animate.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="assets/css/layerslider.css">
	<link rel="stylesheet" type="text/css" href="assets/css/hover-min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/homepage.css">
	<link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="style.css"> -->

	<!-- Core Scripts -->
	<!-- <script type="text/javascript" src="assets/js/jquery.min.js"></script> -->
</head>
<body class="homepage">
	<header>
		<div class="container-fluid">
			<div class="row" id="top">
				<div class="container">
					<div class="col-md-6 col-sm-6 col-xs-12 hidden-xs contact-info text-left">
						<div class="email"><i class="fa fa-envelope"></i> <?php if ( get_theme_mod( 'email' ) ) echo get_theme_mod( 'email' ); else echo 'youremail@domain.com'; ?></div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 secondary-nav text-right">
						<?php 
						wp_nav_menu( array(
							'menu' => 'secondary-menu',
							'depth' => 3,
							'container' => FALSE,
							'menu_class' => 'list-unstyled')
						);
						?>
					</div>
				</div>
			</div>
			<div class="row" id="top-content">
				<nav class="navbar">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" id="animated-navbar-toggle" class="tcon tcon-menu--xcross hidden-md hidden-sm hidden-lg" aria-label="toggle grid" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="float:right;position:relative;top:5px;">
								<span class="tcon-menu__lines" aria
								-hidden="true"></span>
								<span class="tcon-visuallyhidden">toggle grid</span>
							</button>	
							<a class="navbar-brand" href="/">
								<img src="<?php if ( get_theme_mod( 'logo' ) ) echo get_theme_mod( 'logo' ); else echo get_stylesheet_directory_uri() .'/assets/img/logo-2.png'; ?>">
							</a>
						</div>

						
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<?php
								wp_nav_menu( array(
									'menu' => 'main-menu',
									'depth' => 3,
									'container' => FALSE,
									'menu_class' => 'nav navbar-nav navbar-right',
									'menu_id'	=> 'main-menu',
									'walker' => new wp_bootstrap_navwalker())
								);
							?>	

						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
				</nav>