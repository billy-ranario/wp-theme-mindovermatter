<form role="search" id="searchform" method="get" action="<?php echo site_url() ?>">
	<div class="input-group">
		<label class="hidden" for="s"><?php _e('Search:'); ?></label>
		<input type="text" class="s search-input form-control" placeholder="Search for..." value="<?php the_search_query(); ?>" name="s" id="s" />
		<span class="input-group-btn">
			<button class="btn btn-info" id="searchsubmit" type="submit" style="border-radius: 0;"><i class="fa fa-search"></i>&nbsp; Search</button>
		</span>
	</div>
</form>