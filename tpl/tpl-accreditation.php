<?php  
/*
Template Name: Accreditation Page Template 
*/
get_header();
wp_enqueue_style( 'page-css' , get_stylesheet_directory_uri() . '/' . 'assets/css/page.css' );

?>
	<section id="main-container"> <!-- #main-container -->
		<div class="sliding-banner-wrapper">
			<div id="page-banner-content">
				<div class="page-title">
					<div class="container">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="main-content-wrapper"> <!-- #main-content-wrapper -->
				<div class="row">
					<div class="col-md-9 left-content-wrapper">
						<section class="single-content featured-content-wrapper"> <!-- .featured-content-wrapper -->
							<article>
								<?php  
								/**
								* @param this displays the page content
								*/	
								echo page_content();							

								/**
								* @param this displays accreditation images
								*/	
								echo display_bottom_img();
								?>
								<span class="bg-url" route="<?php echo get_field('background_image'); ?>" default="<?php echo get_stylesheet_directory_uri() ?>/assets/img/slides/DSCN0387.jpg"></span>
							</article>
						</section> <!--End: .news-content-wrapper -->						
					</div>
					<?php 
					/**
					* @param this displays sidebar
					*/	
					get_sidebar();				 
					?>				
				</div>
				<br><br>
				<section class="single-content partners-content-wrapper" hidden> <!-- .partners-content-wrapper -->
					<div class="section-heading text-center wow fadeIn">
						<label>Our Partners</label>
						<p><small>Here are the list of our trusted partners</small></p>
						<div class="stars">
							<div class="star-line">
								<div class="star-bg">
									<div class="stars-img"></div>
								</div>
							</div>
						</div>
					</div>
					<?php  
					/**
					* @param this displays the bottom images from functions-custom.php
					*/
					echo display_bottom_img();
					?>
				</section> <!--End: .partners-content-wrapper -->

			</div> <!-- End: #main-content-wrapper -->
		</div>	

		<?php  
		/*
		* Gets the template part 
		* Modal Contact Form 
		*/
		get_template_part( 'tpl/tpl' , 'parallax-contact-window' );
		?>

	</section> <!-- End: #main-container -->

<?php get_footer(); ?>