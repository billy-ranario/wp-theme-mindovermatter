		<div class="container-fluid remove-side-padding">
			<section class="single-content parallax-window-wrapper">
				<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_stylesheet_directory_uri() . '/'; ?>assets/img/slider-bg2.jpg">
					<div class="container text-center">
						<h1 class=" wow fadeInUp">SEND US YOUR INQUIRY</h1>
						<p class=" wow fadeInUp">
							<small>We do things professionally and we are top in what we do.</small>
						</p>
						<a href="javascript:void()" class="btn btn-dark btn-lg wow fadeInUp" data-toggle="modal" data-target="#contact-form-modal"><i class="fa fa-send"></i> <span>INQUIRE NOW</span></a>
					</div>
				</div>
			</section>
		</div>


		<!-- Modal -->
		<div class="modal fade" id="contact-form-modal" tabindex="-1" role="dialog" aria-labelledby="contact-form-modal">
			<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Send us your inquiry</h4>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control">
						</div>
						<div class="form-group">
							<label>Contact # (Required)</label>
							<input type="text" class="form-control" required>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp;Send</button>
				</div>
			</div>
			</div>
		</div>
