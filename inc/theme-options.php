<?php
function customize_options_register($wp_customize) {
	$themeurl = get_template_directory_uri().'/';
	
/*---- Start THEME LOGO ----*/
	// $wp_customize->add_section('theme_header', array(
	// 	'title' => __('Header', 'billy-theme'),
	// 	'description' => 'Modify the theme logo',
	// 	'priority'   => 32
	// ));
	// $wp_customize->add_setting('favicon', array(
	// 	'default' => $themeurl.'images/favicon.png'
	// ));
	// $wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'favicon', array(
	// 	'label' => __('Set Favicon', 'billy-theme'),
	// 	'section' => 'theme_header', 
	// 	'settings' => 'favicon'
	// ) ));
/*---- End THEME LOGO ----*/

/*---- SERVICE HEADING TITLE ----*/
	$wp_customize->add_section('theme_company_service_heading', array(
		'title' => __('SERVICES SECTION HEADING', 'billy-theme'),
		'description' => 'Customize services heading',
		'priority'   => 32
	));
	// Heading Title
	$wp_customize->add_setting('section_title', array(
		'default' => 'Add welcome Title here..'
	));
	$wp_customize->add_control('section_title', array(
		'label' => __('Write heading title/text in this section', 'billy-theme'),
		'section' => 'theme_company_service_heading', 
		'settings' => 'section_title'
	));
	
	// Sub-Heading Title
	$wp_customize->add_setting('section_subheading', array(
		'default' => 'Add subheading here..'
	));
	$wp_customize->add_control('section_subheading', array(
		'label' => __('Write subheading title/text in this section', 'billy-theme'),
		'section' => 'theme_company_service_heading', 
		'settings' => 'section_subheading'
	));
	

/*---- End SERVICE HEADING TITLE ----*/
	
/*---- SERVICE 1 ----*/
	$wp_customize->add_section('theme_company_service_1', array(
		'title' => __('SERVICE 1', 'billy-theme'),
		'description' => 'Modify your company services.',
		'priority'   => 32
	));
	/* Font Awesome Icons */
	$wp_customize->add_setting('fa_icon_1', array(
		'default' => 'plane'
	));
	$wp_customize->add_control('fa_icon_1', array(
		'label' => __('Service Icon', 'billy-theme'),
		'description' => 'Get icon keywords from https://fortawesome.github.io/Font-Awesome/icons/',
		'section' => 'theme_company_service_1', 
		'settings' => 'fa_icon_1'
	));
	/* Service Title Label */
	$wp_customize->add_setting('label_1', array(
		'default' => 'Lorem Ipsum'
	));
	$wp_customize->add_control('label_1', array(
		'label' => __('Service Title Label', 'billy-theme'),
		'section' => 'theme_company_service_1', 
		'settings' => 'label_1'
	));
	/* Service content description */
	$wp_customize->add_setting('description_1', array(
		'default' => 'Add your service description here.'
	));
	$wp_customize->add_control('description_1', array(
		'label' => __('Service content description', 'billy-theme'),
		'section' => 'theme_company_service_1', 
		'settings' => 'description_1',
		'type'    => 'textarea'
	));
/*---- End SERVICE 1 ----*/
	
/*---- SERVICE 2 ----*/
	$wp_customize->add_section('theme_company_service_2', array(
		'title' => __('SERVICE 2', 'billy-theme'),
		'description' => 'Modify your company services.',
		'priority'   => 32
	));
	/* Font Awesome Icons */
	$wp_customize->add_setting('fa_icon_2', array(
		'default' => 'calendar-check-o'
	));
	$wp_customize->add_control('fa_icon_2', array(
		'label' => __('Service Icon', 'billy-theme'),
		'description' => 'Get icon keywords from https://fortawesome.github.io/Font-Awesome/icons/',
		'section' => 'theme_company_service_2', 
		'settings' => 'fa_icon_2'
	));
	/* Service Title Label */
	$wp_customize->add_setting('label_2', array(
		'default' => 'Lorem Ipsum'
	));
	$wp_customize->add_control('label_2', array(
		'label' => __('Service Title Label', 'billy-theme'),
		'section' => 'theme_company_service_2', 
		'settings' => 'label_2'
	));
	/* Service content description */
	$wp_customize->add_setting('description_2', array(
		'default' => 'Add your service description here.'
	));
	$wp_customize->add_control('description_2', array(
		'label' => __('Service content description', 'billy-theme'),
		'section' => 'theme_company_service_2', 
		'settings' => 'description_2',
		'type'    => 'textarea'
	));
/*---- End SERVICE 2 ----*/
	
/*---- SERVICE 3 ----*/
	$wp_customize->add_section('theme_company_service_3', array(
		'title' => __('SERVICE 3', 'billy-theme'),
		'description' => 'Modify your company services.',
		'priority'   => 32
	));
	/* Font Awesome Icons */
	$wp_customize->add_setting('fa_icon_3', array(
		'default' => 'dropbox'
	));
	$wp_customize->add_control('fa_icon_3', array(
		'label' => __('Service Icon', 'billy-theme'),
		'description' => 'Get icon keywords from https://fortawesome.github.io/Font-Awesome/icons/',
		'section' => 'theme_company_service_3', 
		'settings' => 'fa_icon_3'
	));
	/* Service Title Label */
	$wp_customize->add_setting('label_3', array(
		'default' => 'Lorem Ipsum'
	));
	$wp_customize->add_control('label_3', array(
		'label' => __('Service Title Label', 'billy-theme'),
		'section' => 'theme_company_service_3', 
		'settings' => 'label_3'
	));
	/* Service content description */
	$wp_customize->add_setting('description_3', array(
		'default' => 'Add your service description here.'
	));
	$wp_customize->add_control('description_3', array(
		'label' => __('Service content description', 'billy-theme'),
		'section' => 'theme_company_service_3', 
		'settings' => 'description_3',
		'type'    => 'textarea'
	));
/*---- End SERVICE 3 ----*/
	
/*---- SERVICE 4 ----*/
	$wp_customize->add_section('theme_company_service_4', array(
		'title' => __('SERVICE 4', 'billy-theme'),
		'description' => 'Modify your company services.',
		'priority'   => 32
	));
	/* Font Awesome Icons */
	$wp_customize->add_setting('fa_icon_4', array(
		'default' => 'credit-card'
	));
	$wp_customize->add_control('fa_icon_4', array(
		'label' => __('Service Icon', 'billy-theme'),
		'description' => 'Get icon keywords from https://fortawesome.github.io/Font-Awesome/icons/',
		'section' => 'theme_company_service_4', 
		'settings' => 'fa_icon_4'
	));
	/* Service Title Label */
	$wp_customize->add_setting('label_4', array(
		'default' => 'Lorem Ipsum'
	));
	$wp_customize->add_control('label_4', array(
		'label' => __('Service Title Label', 'billy-theme'),
		'section' => 'theme_company_service_4', 
		'settings' => 'label_4'
	));
	/* Service content description */
	$wp_customize->add_setting('description_4', array(
		'default' => 'Add your service description here.'
	));
	$wp_customize->add_control('description_4', array(
		'label' => __('Service content description', 'billy-theme'),
		'section' => 'theme_company_service_4', 
		'settings' => 'description_4',
		'type'    => 'textarea'
	));
/*---- End SERVICE 4 ----*/
	
/*--- Start THEME COPYRIGHT and CREDIT ---*/
	$wp_customize->add_section('theme_copyright_credit', array(
		'title' => __('Copyright and Credit', 'billy-theme'),
		'description' => 'Add theme Copyright and Credit'
	));
	/*--- Copyright ---*/
	$wp_customize->add_setting('copyright', array(
		'default' => 'Copyright 2015'
	));
	$wp_customize->add_control('copyright', array(
		'label' => __('Set Theme Copyright here', 'billy-theme'),
		'section' => 'theme_copyright_credit', 
		'settings' => 'copyright'
	));
	/*--- Credit ---*/
	$wp_customize->add_setting('credit', array(
		'default' => 'naturopathicvirtualassistant.com'
	));
	$wp_customize->add_control('credit', array(
		'label' => __('Set Theme Credit/Developed by', 'billy-theme'),
		'section' => 'theme_copyright_credit', 
		'settings' => 'credit'
	));

	/*--- Footer Content ---*/
	$wp_customize->add_section('theme_footer_desctription', array(
		'title' => __('Add Blog description', 'billy-theme'),
		'description' => 'Footer Description'
	));	
	// Title
	$wp_customize->add_setting('footer_title', array(
		'default' => ''
	));
	$wp_customize->add_control('footer_title', array(
		'label' => __('Add blog owner', 'billy-theme'),
		'section' => 'theme_footer_desctription', 
		'settings' => 'footer_title'
	));
	// Description
	$wp_customize->add_setting('footer_description', array(
		'default' => ''
	));
	$wp_customize->add_control('footer_description', array(
		'label' => __('Add blog owner description', 'billy-theme'),
		'section' => 'theme_footer_desctription', 
		'settings' => 'footer_description'
	));

/*--- End THEME COPYRIGHT and CREDIT ---*/


/*---------------- ADD SOCIAL LINKS ----------------*/
	$wp_customize->add_section('theme_social_links', array(
		'title' => __('Social Links', 'billy-theme'),
		'description' => 'Add Social Links details.'
	));
		/*--- Facebook ---*/
	$wp_customize->add_setting('facebook', array(
		'default' => 'https://www.facebook.com/'
	));
	$wp_customize->add_control('facebook', array(
		'label' => __('Enter your Facebook link', 'billy-theme'),
		'section' => 'theme_social_links', 
		'settings' => 'facebook'
	));
		/*--- Twitter ---*/
	$wp_customize->add_setting('twitter', array(
		'default' => 'https://twitter.com/'
	));
	$wp_customize->add_control('twitter', array(
		'label' => __('Enter your Twitter link', 'billy-theme'),
		'section' => 'theme_social_links', 
		'settings' => 'twitter'
	));
		/*--- Youtube ---*/
	$wp_customize->add_setting('youtube', array(
		'default' => 'https://youtube.com/'
	));
	$wp_customize->add_control('youtube', array(
		'label' => __('Enter your youtube link', 'billy-theme'),
		'section' => 'theme_social_links', 
		'settings' => 'youtube'
	));
/*---------------- end: ADD SOCIAL LINKS ----------------*/


/*--- Start CONTACT Information ---*/
	$wp_customize->add_section('theme_contact_info', array(
		'title' => __('Contact Information', 'billy-theme'),
		'description' => 'Add Contact Information details.'
	));
	/*--- Address ---*/
	$wp_customize->add_setting('address', array(
		'default' => 'Your Address, Here'
	));
	$wp_customize->add_control('address', array(
		'label' => __('Enter your address here', 'billy-theme'),
		'section' => 'theme_contact_info', 
		'settings' => 'address',
		'type' => 'textarea'
	));
	/*--- Phone Number ---*/
	$wp_customize->add_setting('phone', array(
		'default' => '00 00 00 00'
	));
	$wp_customize->add_control('phone', array(
		'label' => __('Enter your phone number here', 'billy-theme'),
		'section' => 'theme_contact_info', 
		'settings' => 'phone'
	));
	/*--- Fax Number ---*/
	$wp_customize->add_setting('fax', array(
		'default' => '00 00 00 00'
	));
	$wp_customize->add_control('fax', array(
		'label' => __('Enter your fax number here', 'billy-theme'),
		'section' => 'theme_contact_info', 
		'settings' => 'fax'
	));

	/*--- Email ---*/
	$wp_customize->add_setting('email', array(
		'default' => 'mail@example.com'
	));
	$wp_customize->add_control('email', array(
		'label' => __('Enter your email here', 'billy-theme'),
		'section' => 'theme_contact_info', 
		'settings' => 'email'
	));
/*--- End CONTACT Information ---*/
}

add_action('customize_register', 'customize_options_register');
?>
