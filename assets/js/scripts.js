$( document ).ready( function() {

	var $document = $(document),
		$element = $('header'),
		className = 'hasScrolled';

	$document.scroll(function() {
		if ($document.scrollTop() >= 250) {
			$element.addClass(className);
			$( '.scrolltop-wrapper' ).fadeIn();
		} else {
			$element.removeClass(className);
			$( '.scrolltop-wrapper' ).fadeOut();
		}
	});	


   // Transformicon Plugin
    transformicons.add('.tcon');

	// Layer Slider 
	$( '#banner' ).layerSlider({
	 	autostart: true,
		skin: 'v5',
		skinsPath: window.location.origin + '/wp-content/themes/genesis-child/assets/skins/',
		responsiveUnder: 960,
		layersContainer: 960,
		hoverBottomNav: true,
		thumbnailNavigation : 'hover'
    });

} );