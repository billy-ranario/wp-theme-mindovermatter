	<footer>
		<div id="f-top">
			<div class="container">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="f-title">
						<h2>Mentoring</h2>
					</div>
					<div class="f-content">
						<ul>
							<?php
							$args = array( 'posts_per_page' => 7, 'category_name' => 'mentoring' );

							$myposts = get_posts( $args );
							foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
								<li>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</li>
							<?php endforeach; 
							wp_reset_postdata();?>
						</ul>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="f-title">
						<h2>Community Events</h2>
					</div>
					<div class="f-content">
						<ul>
							<?php
							$args = array( 'posts_per_page' => 7, 'category_name' => 'Community Events' );

							$myposts = get_posts( $args );
							foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
								<li>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</li>
							<?php endforeach; 
							wp_reset_postdata();?>
						</ul>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="f-title">
						<h2>Connect With Us</h2>
					</div>
					<div class="f-content">
						<p><i class="fa fa-facebook"></i> &nbsp; Like us on <a href="<?php if ( get_theme_mod( 'facebook' ) ) echo get_theme_mod( 'facebook' ); else echo '#'; ?>">Facebook</a></p>
						<p><i class="fa fa-twitter"></i> &nbsp; Follow us <a href="<?php if ( get_theme_mod( 'twitter' ) ) echo get_theme_mod( 'twitter' ); else echo '#'; ?>">Twitter</a></p>
						<p><i class="fa fa-mobile"></i> &nbsp; <?php if ( get_theme_mod( 'mobile' ) ) echo get_theme_mod( 'mobile' ); else echo '+7 988 71 150 / +7 988 30 20'; ?></p>
						<p><i class="fa fa-envelope"></i> &nbsp; <?php if ( get_theme_mod( 'email' ) ) echo get_theme_mod( 'email' ); else echo 'youremail@yourdomain.com'; ?></p>	
						<p><i class="fa fa-map-marker"></i> &nbsp; <?php if ( get_theme_mod( 'address' ) ) echo get_theme_mod( 'address' ); else echo 'Sample Address, Sample City'; ?></p>
					</div>
				</div>
			</div>
		</div>
		<div id="copyright">
			<div class="container">
				<p class="text-center">&copy; Copyright <?php bloginfo( 'name' ); ?>. 2016</p>
			</div>
		</div>
	</footer>

	<div class="scrolltop-wrapper" hidden>
		<a onclick="$('html').animatescroll({scrollSpeed:700,easing:'swing'});" class="btn-arrow-up">
			<i class="fa fa-arrow-up"></i>
		</a>
	</div>

	<?php wp_footer(); ?>

</body>
</html>

<!-- 	<footer>
		<div class="container-fluid" id="secondary-level-footer">
			<div class="row">
				<div class="container">
					<div class="footer-navigation">
						<div class="col-md-4 footer-box  wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay=".5s">
							<div class="footer-logo-wrapper">
								<img src="<?php echo get_stylesheet_directory_uri() . '/'; ?>assets/img/logo.png">
							</div>
							<div class="facebook-like-wrapper" style="margin-top:20px;">
								<p>Like us on our <a href="#">Facebook Page.</a></p>
							</div>
						</div>
						<div class="col-md-4 footer-box  wow fadeInLeft" data-wow-duration="1.4s" data-wow-delay=".2s">
							<div class="content-details">
								<h3>Our Services</h3>
								<ul class="list-unstyled">
									<?php 
										/**
										* This displays the sub-menu pages of Service page
										* @package Creative360Travel Theme
										*/
										$args = array(
											'child_of'     => 16, //Page ID of the parent menu
											'depth'        => 0,
											'echo'         => 1,
											'link_before'  => '<i class="fa fa-angle-double-right"></i>&nbsp;',
											'post_type'    => 'page',
											'post_status'  => 'publish',
											'sort_column'  => 'menu_order, post_title',
											'title_li'     => false, 
											'walker'       => new Walker_Page
										); 

										wp_list_pages( $args );
									?>							
								</ul>
							</div>
						</div>
						<div class="col-md-4 footer-box  wow fadeInLeft" data-wow-duration="1.6s">
							<div class="content-details">
								<h3>Contact Us</h3>
								<p><i class="fa fa-phone-square"></i>&nbsp;<?php if ( get_theme_mod( 'phone' ) ) echo get_theme_mod( 'phone' ); else echo '(123) 4560 789'; ?></p>
								<p><i class="fa fa-envelope"></i>&nbsp; <?php if ( get_theme_mod( 'email' ) ) echo get_theme_mod( 'email' ); else echo 'youremail@yourdomain.com'; ?></p>							
								<p><i class="fa fa-map-marker"></i>&nbsp; <?php if ( get_theme_mod( 'address' ) ) echo get_theme_mod( 'address' ); else echo 'Sample Address, Sample City'; ?></p>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid" id="copyright">
			<div class="row">
				<div class="container footer-text-wrapper">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<p class="remove-margin text-left">
							Copyright &copy; 2015, <a href="#">Creative 360 Travel and Tours</a>
						</p>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<p class="remove-margin text-right">
							Designed and developed by <a href="http://ohsomedevs.com" target="__blank">ohsomedevs.com</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer> -->